﻿# **Tutorial 5: Array dan ArrayList**

Dasar-dasar Pemrograman 2 - CSGE601021 | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Genap 2017/2018

**Dibuat oleh NA | NN**

* * *
Pada tutorial sebelumnya, Anda telah mempelajari konsep OOP dan overloading di Java. Kali ini, Anda akan mempelajari mengenai Array dan ArrayList.

## **Tujuan Pembelajaran**
1. Memahami Array
2. Memahami ArrayList


### **Before You Start...**
1. Lakukan `git pull upstream master` dengan menggunakan Command Prompt pada folder repository lokal Anda.
2. Kerjakan soal tersebut kemudian letakkan file jawaban anda di folder `lab_5/src/main/java`. Kami sudah menyediakan folder kosong untuk kamu.
3. Setelah selesai mengimplementasikan kode tersebut, lakukan add, commit, dan push code kalian tersebut.

## **Materi Tutorial**

### **Array**
**Array** adalah struktur data paling sederhana yang disediakan oleh bahasa pemrograman Java. Secara informal, array **array** adalah sebuah kumpulan n buah data dengan tipe data yang sama​. Selanjutnya, dapat dikatakan bahwa array **array** memiliki panjang n​. Sederhana, bukan?.  Tipe data yang bisa menggunakan Array adalah **primitive** dan **refference/object**. 

**Array bersifat fixed-size** yang berarti panjang sebuah array tidak dapat diubah setelah array tersebut diinstansiasi. Sebuah array dapat diidentifikasi dengan simbol “[]”. Elemen-elemen pada array dapat diakses berdasarkan index/posisi elemen tersebut. Sebagai contoh, array[i] akan mengembalikan elemen dengan index i​ pada array *array*​, tentu saja dengan i < n​, index pada array dimulai dari 0. 

**Array** juga dapat berupa **Array Multidimensi**, yang berarti tiap elemennya merupakan sebuah array juga.

Untuk mengetahui lebih lanjut tentang **array** bisa lihat [disini](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html)

### **Ayo coba tebak hasil dari setiap code dibawah ini untuk menambah pemahaman kamu!!**

**Soal 1**
```java
int[] numbers = new int[10]; // menginisiasi array of int dengan panjang 10 
System.out.println(numbers[5]); // menampilkan ... , karena ...

```
**Soal 2**
```java
String[] strList = new String[5];//...
System.out.println(strList[0]);// menampilkan ... , karena ....

```
**Soal 3**
```java
int[] primes = {2,3,5,7,11}; //menginisiasi array of int dengan initial values 
System.out.println(primes[2]);// menampilkan ...

```
**Soal 4**
```java
String[] animals = {"cat","bird","tiger"}; // ...
System.out.println(animals[1]); // menampilkan ...
animals[0] = "fish"; //mengganti elemen index ke 0
System.out.println(animals[1]); // menampilkan ...

```
**Soal 5**
```java
String str  = "a b c d";
String[] splitstr = str.split(" "); //menginisiasi array of String dari method split
System.out.println(splitstr[3]);// menampilkan ...
splitstr[3] = "new"; //...

```
**Soal 6**
```java
int[][] points = new int[4][4];     // array of int 2 dimensi dengan panjang 4, dan lebar 4 seperti papan catur

System.out.println(points[0][0]);  //....

points[0][1] = 5;  //...
points[1] = new int[2]; //...
points[2] = {1,2,3};    //...
Int[] sesuatu = {1,2,3};  //....

System.out.println(points[0][5]); //...

```


### **ArrayList**
**ArrayList** sederhananya merupakan array yang dinamik, berbeda dengan array, ukuran ArrayList dapat berkembang sesuai kebutuhan, dan elemen-elemen yang disimpan ArrayList harus bertipe reference.

Untuk mengetahui lebih lanjut tentang **array** bisa lihat [disini](https://www.tutorialspoint.com/java/java_arraylist_class.htm)

### **Ayo cari tahu apa yang dilakukan code dibawah ini untuk menambah pemahaman kamu!!**

```java
import java.util.ArrayList;
public class Test {
    public static void main(String[] args){
    	ArrayList<Integer> numbers = new ArrayList<Integer>(); // menginisiasi arraylist of integer

    	//.......
    	numbers.add(3); 
		numbers.add(2);
		numbers.add(4);
		numbers.add(5);
		numbers.add(7);


		numbers.size(); //...
		numbers.get(0); //...
		numbers.set(0, 1); //....


		numbers.remove(1); //....

	}
}
```



## **Soal Tutorial : "Bingo? BINGO!"**

![alt text](https://myfreebingocards.com/bingo-card-generator/results?not-random=1&img=1&title=BINGO&words=02%0D%0A04%0D%0A10%0D%0A39%0D%0A53%0D%0A29%0D%0A13%0D%0A77%0D%0A42%0D%0A55%0D%0A31%0D%0A73%0D%0A88%0D%0A69%0D%0A32%0D%0A40%0D%0A86%0D%0A30%0D%0A01%0D%0A27%0D%0A08%0D%0A49%0D%0A52%0D%0A81%0D%0A17&theme=stars-and-stripes&size=5&per-page=1&free-space-text=FREE+SPACE&s=1 "Bingo Card")


------

### **What's the story?**

Suatu hari, Dek Depe ditawari oleh Sis Dea untuk membuat suatu permainan. Dek Depe diminta untuk membuat permainan Bingo dengan ukuran 5x5, dimana objektifnya untuk menghasilkan garis lurus dari 5 angka, baik secara horizontal, vertikal, maupun diagonal. Depe merasakan bahwa ia dapat membentuk permainan tersebut cukup menggunakan array biasa yang baru ia pelajari di java, dan dengan bantuan teman - temannya para pengikut kuliah DDP2 yang baik hati dan rajin menabung ia pun yakin ia dapat melakukannya!

Bingo merupakan permainan di papan yang memiliki grid 5x5 dengan tiap slot memiliki suatu angka. Dalam permainannya, suatu angka akan diberitahu secara acak, jika angka itu terdapat dalam papanmu maka angka tersebut akan ditandai. Permainan akan berakhir apabila seseorang berhasil menandai 5 angka berturut-turut yang menghasilkan garis lurus baik secara vertikal, horizontal, maupun diagonal.

### **Spesifikasi Class**

Karena minggu lalu sudah belajar mengenai OOP, diharapkan mulai sekarang untuk membiasakan mengerjakan tutorial dengan menggunakan konsep OOP jika dibutuhkan sehingga main method hanya digunakan untuk mengolah data atau memanggil method class yang telah dibuat.

Dalam pengerjaan tutorial, diharapkan membuat class BingoCard.java (anda boleh membuat class lain jika dibutuhkan) , implementasi dapat sekreatif anda dan membuat method sesuai kebutuhan anda dengan setidaknya terdapat method:
- [ ] markNum
- [ ] info
- [ ] restart

### Flow Program 
Awal inisiasi program dimulai dengan menerima 5 baris inputan dari user yang terdiri dari 5 angka terpisah dengan spasi. 5 baris tersebut akan menghasilkan sebanyak 25 angka yang akan digunakan sebagai kartu bingo anda. Ketika inisiasi tersebut telah dilaksanakan, permainan dapat dimulai yang akan secara terus menerus meminta perintah dari user. Perintah yang dapat diberikan merupakan sebagai berikut :

### MARK (Angka)

Perintah tersebut akan menandakan/menyilangkan angka tersebut yang ada di kartu anda
Setelah berhasil menandakan angka, program akan mengecek apakah berhasil terbentuknya garis lurus(diagonal termasuk) dari 5 angka yang menghasilkan bingo. Jika terjadi bingo maka permainan selesai dan program akan berhenti menerima input. 

Jika angka tersebut tidak ada di kartu, program akan mengeluarkan output : ”Kartu tidak memiliki angka (Angka)”
Jika angka tersebut ada di kartu dan belum tersilang, program akan mengeluarkan output : ”(Angka) tersilang”
Jika angka tersebut sebelumnya sudah tersilang, program akan mengeluarkan output : ”(Angka) sebelumnya sudah tersilang”
Setelah menandakan suatu angka, apabila terbentuk BINGO maka program akan mengeluarkan output “BINGO!”

### INFO

Program akan mencetak kondisi kartu sementara. Angka - angka yang telah disilang akan diganti dengan “X”

Contoh output :

| 46 | 37 | 83 | 12 | 17 |

| 04 | X  | 20 | 22 | 39 |

| X  | 73 | 44 | 51 | 90 |

| 13 | 05 | 81 | X  | 55 |

| 26 | 58 | 91 | X  | 34 |

### RESTART

Mengulang permainan dengan menggunakan kartu yang sama. Semua angka yang telah disilang dikembalikan ke kondisi semula

Program akan mengeluarkan output “Mulligan!”

 
Jika perintah yang diberikan bukan merupakan antara 3 tersebut program akan mengeluarkan output “Incorrect command”

Ketika bingo tercapai, program akan berhenti menerima inputan dan mengeluarkan pesan “BINGO!” dan mencetak kondisi kartu.


## Contoh Input

10 11 12 13 14

29 28 27 26 25

31 33 35 37 39

42 44 40 46 48

58 64 71 82 99

MARK 11

MARK 44

MARK 64

MARK 59

MARK 27

INFO

MARK 28

MARK 44

RESTART

MARK 44

MARK 35

MARK 14

MARK 58

INFO

MARK 26

## Contoh Output

11 tersilang

44 tersilang

64 tersilang

Kartu tidak memiliki angka 59

27 tersilang

| 10 | X  | 12 | 13 | 14 | 

| 29 | 28 | X  | 26 | 25 | 

| 31 | 33 | 35 | 37 | 39 | 

| 42 | X  | 40 | 46 | 48 | 

| 58 | X  | 71 | 82 | 99 | 

28 tersilang

44 sebelumnya sudah tersilang

Mulligan!

44 tersilang

35 tersilang

14 tersilang

58 tersilang

| 10 | 11 | 12 | 13 | X  | 

| 29 | 28 | 27 | 26 | 25 | 

| 31 | 33 | X  | 37 | 39 | 

| 42 | X  | 40 | 46 | 48 | 

| X  | 64 | 71 | 82 | 99 | 

26 tersilang

BINGO!

| 10 | 11 | 12 | 13 | X  | 

| 29 | 28 | 27 | X  | 25 | 

| 31 | 33 | X  | 37 | 39 | 

| 42 | X  | 40 | 46 | 48 | 

| X  | 64 | 71 | 82 | 99 | 



>Notes:
- Pengerjaan soal diharapkan hanya menggunakan Array tanpa ArrayList
- Angka yang diberikan dipastikan unik, tidak akan ada angka yang sama didalam kartu.
- Angka yang diberikan maksimal 2 digit.
- Semua input menggunakan format yang benar sehingga tidak diperlukan untuk membuat handler input yang salah





## **Soal Bonus: "Lelah single(player) terus :("**
Untuk soal bonus ini, kalian sekarang diharapkan mengimplementasi MultiPlayer Mode! Mirip flow program soal utama, hanya saja sebelum menerima inputan angka yang membentuk papan bingomu, program akan pertama kali diharapkan menerima 1 baris dengan format suatu angka dilanjutkan dengan nama - nama n kali( "N Nama-1 Nama-2 ... Nama-N") . N merepresentasikan jumlah pemain, dan Nama yang diterima merupakan nama pemain tersebut. Setelah menerima baris tersebut program akan meminta 5 barisan angka seperti biasa sebanyak n kali untuk tiap pemain.

Untuk Input Output terdapat sedikit modifikasi juga.

### MARK (Angka)
Semua pemain akan melaksanakan perintah MARK, hanya saja output hanya menunjukkan pemain - pemain yang berhasil menyilangkan angka secara berurutan sesuai urutan ketika nama diberikan. Contoh:

"(Nama1): (Angka) tersilang!"

"(Nama2): Kartu tidak memiliki angka (Angka)”

"(Nama3): (Angka) sebelumnya sudah tersilang”

"(Nama4): (Angka) tersilang!"

"(Nama5): (Angka) tersilang!"



### INFO (Nama)
Program akan mencetak nama pemain dan kondisi kartu milik nama tersebut.

Contoh output :

(Nama)

| 46 | 37 | 83 | 12 | 17 |

| 04 | X  | 20 | 22 | 39 |

| X  | 73 | 44 | 51 | 90 |

| 13 | 05 | 81 | X  | 55 |

| 26 | 58 | 91 | X  | 34 |

### RESTART (Nama)
Fungsi tersebut sekarang akan mengembalikan kondisi semula untuk semua pemain, tetapi tiap pemain hanya boleh melakukan restart 1 kali.
Jika pemain memanggil restart lebih dari 1 kali, program hanya akan mengeluarkan output "(Nama) sudah pernah mengajukan RESTART" tanpa mengubah kondisi kartu.

Ketika kartu suatu pemain telah mencapai kondisi menang, maka program akan mencetak nama pemain tersebut dan kondisi kartunya dan berhenti. (Pemenang dapat lebih dari 1 orang)


## Checklist
Isi kurung siku komponen dengan x untuk menceklis komponen.
### Komponen Wajib | 100 Poin
- [ ] **Membuat kelas BingoCard sesuai spesifikasi**
- [ ] **Melakukan setup awal permainan**
- [ ] **Mengimplementasikan method markNum**
- [ ] **Mengimplementasikan method info**
- [ ] **Mengimplementasikan method restart**
- [ ] **Memberhentikan program ketika pemain telah menang dan mencetak status akhir kartu**

### Komponen Bonus | 10 Poin
- [ ] **Memungkinkan terdapat pemain lebih dari 1**
- [ ] **Merubah method markNum**
- [ ] **Merubah method info**
- [ ] **Merubah method restart**
- [ ] **Mencetak pemenang dari permainan dan kondisi kartunya**

-----
### **Woah, apa ini !?**

Ketika kalian meng-push hasil kerja kalian, kalian akan sadar bahwa ada logo cross merah atau centang hijau di samping hasil kerja kalian.

![alt text](https://i.imgur.com/ZNfetmP.png "Ilustrasi git 1")

Kalian mungkin memperhatikan bahwa kita mulai memakai sistem git sejak semester 2, mengikuti kakak angkatan kalian yang baru mulai memakai sistem git sejak semester 3. Salah satu guna dari menggunakan git adalah kita bisa menggunakan fitur Continuos Integration?

Apa itu Continuous Integration? Continuos Integration adalah konsep di mana ketika kalian push, hasil push kalian langsung di build (compile) dan di test (uji langsung). Jika gagal, kalian akan diberitahu.

Bagian Build baru akan dijelaskan di mata kuliah Advanced Programming. Kalian hanya perlu mengetahui bagian test.

Sistem yang digunakan untuk mengetest di Java bernama JUnit. Kita bisa menggunakan framework JUnit untuk mengetes secara langsung (tanpa harus print di command line). Untuk sekarang, kalian tidak perlu tahu cara kerja JUnit.

Kamu dapat memeriksa hasil kerja Junit di tab Commit. Tekan logo centang hijau atau cross merah untuk memeriksa detail lebih lanjut.

![alt text](https://i.imgur.com/E23AOfl.png "Ilustrasi commit")

Ketika kamu menekan logo tersebut, kamu akan memeriksa rangkuman dari tes tersebut yang memiliki dua lingkaran.

Jika lingkaran pertama cross, maka program kamu gagal karena compile error.
Jika lingkaran pertama centang hijau tetapi lingkaran kedua cross, maka program kamu tidak akurat.
Jika kedua lingkaran centang, berati program kamu sudah baik.

![alt text](https://i.imgur.com/1ElduFi.png "Ilustrasi status")

Kamu dapat menekan tombol cross merah atau centang hijau untuk melihat hasil lebih lanjut. Sebagai contoh, jika kalain mendapat cross merah di lingkaran kedua, kamu dapat menemkan cross merah kedua untuk melihat test case mana program kalian tidak akurat.
