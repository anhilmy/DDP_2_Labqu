import java.util.Scanner;

/*
Codingan Java  Rumah Kelinci, WIBU LU!
Ahmad Naufal Hilmy, DDP F, 1706043613, gitlab : ahmadnhilmy
*/

public class RabbitHouse{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		String mode = input.next();			//input awal berupa mode yang diinginkan
		mode = mode.toLowerCase();			//mengubah semua karakter menjadi lowercase
		String name = input.next();			//input kedua adalah nama kelinci
		int len = name.length();	
		if(mode.equals("normal")){						//cek mode
 			System.out.println(KelinciBeranak(len, 1));    //memanggil method KelinciBeranak(panjang nama, nomor rekursi)
		}else if(mode.equals("palindrom")){				//cek mode
			System.out.println(modePalindrome(name));
		}
		//kalau bukan dua duanya
		else{											
			System.out.println("Input salah");
		}
		
	}
	private static int KelinciBeranak(int len, int recursv){
		if(len==recursv){
			return 1;
		}
		else{
			return 	Factorial(len, recursv) + KelinciBeranak(len, recursv+1);   
			//len tidak bertambah karena untuk pengali awal, yang bertambah adalah nomor recursif
		}
	}
	
	private static int Factorial(int len, int recursv){ //Pembuat faktorialnya lalu dibagi dengan nomor rekursif dari KelinciBeranak
		if(len<2 | len==recursv){
			return 1;
		}else{
			return len*Factorial(len-1, recursv);
		}
	}
	
	private static int modePalindrome(String name){   //kalau mode yang diinginkan palindrom, program akan masuk ke sini
		if (Palindrome(name)){  //cek ke method palindrom kalau true, dia akan mati
			return 0;
		}else{
			int jumlahKelinci = 1; ////karena tidak bisa return dalam iterasi, maka membuat variable dulu
			for(int a = 0; a < name.length(); a++){  //parameter a untuk slicing string
				jumlahKelinci += modePalindrome(name.substring(0,a)+name.substring(a+1,name.length())); //rekursif untuk pemotongan string
			}
			return (jumlahKelinci);  //return jumlahKelinci
		}
	}
	
	private static boolean Palindrome(String name){ //pengecekan bahwa nama kelinci adalah palindrom
		if(name.length()<2){ //jika panjang nama itu 1 atau 0, maka kelinci akan mati
			return true;
		}else if(name.charAt(0) == name.charAt(name.length()-1)){ 
			return Palindrome(name.substring(1, name.length()-1));   //menggunakan rekursi
		}
		return false;
	}
	
}

			