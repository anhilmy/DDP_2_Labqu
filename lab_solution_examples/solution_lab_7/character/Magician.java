package character;

public class Magician extends Human{
	public Magician(String name, int hp) {
		super(name, hp);
	}
	public String burn(Player korban){
		korban.burned();
		String output = "Nyawa " + korban.getName() + " " + korban.getHp();
		if(korban.isCooked()){
			output += "\n dan matang";
		}
		return output;
	}
	public void damaged(){
		this.hp-=20;
		if(this.hp < 0){
			this.hp = 0;
		}
	}
	public void burned(){
		this.hp -= 20;
		if(this.isDead()){
			this.isCooked = true;
		}
		if(this.hp < 0){
			this.hp = 0;
		}
	}
}