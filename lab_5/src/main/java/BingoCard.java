/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
        */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	private String infoBingo;
	private int bingoCheckCount;
	private Number thisObject;
	private String nama;
	private boolean restartIndicator = false;

	public String getNama(){
		return this.nama;
	}
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}
	public BingoCard(String nama) { //soal bonus inisiasi nama terlebih dahulu
		this.isBingo = false;
		this.nama = nama;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() { return isBingo;	}  //getter Bingo

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num) {
		//kalau nomornya tidak ada akan error nullPointer
		try {
			thisObject = numberStates[num];
			if (thisObject.isChecked()) {
				return (nama +" : " + num + " sebelumnya sudah tersilang");
			}
			thisObject.setChecked(true);
			return (nama + " : " + num + " tersilang");

		} catch (NullPointerException e) {
			return (nama + " : " + "Kartu tidak memiliki angka " + num);
		}
	}
	
	public String info() {
		infoBingo = "";  //setiap kali info() maka akan membuat string ulang
		for (int x = 0; x < 5; x++) {
			infoBingo += "| "; //awal tiap baris
			for (int y = 0; y < 5; y++) {
				if (numbers[x][y].isChecked()) {
					infoBingo += "X ";
				} else {
					infoBingo += String.format("%02d",(numbers[x][y].getValue()));
				}
				if (y != 4) {
					infoBingo += " | ";
				}
			}
			if (x != 4) {
				infoBingo += " |\n";
			} else {
				infoBingo += " |";
			}
		}
		return infoBingo;
	}

	public boolean willRestart(){ //pengecekan apakah player bisa restart
		if (restartIndicator) {
			System.out.println(nama + " sudah pernah mengajukan RESTART");
			return false;
		} else {
			restartIndicator = true;
			return true;
		}
	}

	public void restart() {
		//looping di 2d array
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				numbers[x][y].setChecked(false);
			}
		}
		System.out.println(nama + " : Mulligan!");
	}




	public void bingoChecker(){
		//mengecek bingo secara horizontal
		for (int x = 0; x < 5; x++){
			bingoCheckCount = 0;
			for (int y = 0; y < 5; y++) {
				if(numbers[x][y].isChecked()){
					bingoCheckCount++;
				}
			}
			if(bingoCheckCount==5){
				System.out.println("BINGO!");
				setBingo(true);
				return;
			}
		}

		//mengecek bingo secara vertikal
		for (int y = 0; y < 5; y++) {
			bingoCheckCount = 0;
			for (int x = 0; x < 5; x++) {
				if (numbers[x][y].isChecked()) {
					bingoCheckCount++;
				}
			}
			if (bingoCheckCount == 5) {
				System.out.println("BINGO!");
				setBingo(true);
				return;
			}
		}


		//mengecek secara diagonal 1
		bingoCheckCount = 0;
		for(int xy = 0; xy < 5; xy++) {
			if (numbers[xy][xy].isChecked()) {
				bingoCheckCount++;
			}
			if (bingoCheckCount == 5) {
				System.out.println("BINGO!");
				setBingo(true);
				return;
			}
		}

		//mengecek secara diagonal 2
		bingoCheckCount = 0;
		for(int x = 0; x < 5; x++) {
			if (numbers[x][4-x].isChecked()) {
				bingoCheckCount++;
			}

			if (bingoCheckCount == 5) {
				System.out.println("BINGO!");
				setBingo(true);
				return;
			}
		}
	}
}

