import java.util.Scanner;

public class mainBingo{
    public static void main(String[] args){
        boolean recursv = true;
        Scanner input = new Scanner(System.in);
        int bingoNumber = 0;
        String perintah = "";
        int num = 0;
        byte numPlayer = Byte.parseByte(input.next());
        BingoCard[] card = new BingoCard[numPlayer];


        //inisiasi object bingocard untuk tiap pemain
        for (int i = 0; i < numPlayer; i++) {
            card[i] = new BingoCard(input.next());
        }

        for (BingoCard player : card) {
            System.out.println("bingo card " + player.getNama());
            Number[][] numbers = new Number[5][5];
            Number[] states = new Number[100]; //inisiasi tiap loop agar tiap pemain mendapat awal numberStates yang kosong

            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    bingoNumber = Integer.parseInt(input.next());
                    numbers[i][j] = new Number(bingoNumber, i, j);
                }
            }

            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    states[numbers[i][j].getValue()] = numbers[i][j];
                }
            }

            player.setNumbers(numbers);
            player.setNumberStates(states);
        }

        while(recursv){  //cek terus selama belum ada yang menang
            perintah = input.next();

            if(perintah.equals("RESTART")) {
                String peminta = input.next();
                boolean validasiRestart = false; //menunggu perubahan untuk avaiablelitas yang mau restart

                //cari objek yang meminta restart
                for (BingoCard player : card) {
                    if (player.getNama().equals(peminta)) {

                        //cek apakah dia bisa meminta restart
                        if (player.willRestart()) {
                            validasiRestart = true; //telah tervalidasi dia bisa restart

                        }
                        break;
                    }
                }

                //loop terhadap array card
                if(validasiRestart){
                    for (BingoCard player : card) {
                        player.restart();
                    }
                }


            }else if(perintah.equals("MARK")){
                num = Integer.parseInt(input.next());

                for (BingoCard player : card) { //markNum semua pemain
                    System.out.println(player.markNum(num));
                }

            }else if(perintah.equals("INFO")){
                for (BingoCard player : card) { //looping ke semua pemain .info()
                    System.out.println(player.info());
                }
            }
            for (BingoCard player : card) {
                player.bingoChecker();
                if(player.isBingo()) {
                    System.out.println(player.info());
                    recursv = false;
                }
            }

        }
    }
}