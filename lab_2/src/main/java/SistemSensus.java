import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Hilmy, NPM 1706043613, Kelas F, GitLab Account: ahmadnhilmy
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		try{
			String nama = input.nextLine();
			System.out.print("Alamat Rumah           : ");
			String alamat = input.nextLine();
			System.out.print("Panjang Tubuh (cm)     : ");
			short panjang = Short.parseShort(input.nextLine());
			if(panjang > 250 || panjang < 0){
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				return;
			}
			
			System.out.print("Lebar Tubuh (cm)       : ");
			short lebar = Short.parseShort(input.nextLine());
			if(lebar > 250 || lebar < 1){
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				return;
			}
			
			System.out.print("Tinggi Tubuh (cm)      : ");
			short tinggi = Short.parseShort(input.nextLine());
			if(tinggi > 250 || tinggi < 1){
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				return;
			}
			
			System.out.print("Berat Tubuh (kg)       : ");
			float berat = Float.parseFloat(input.nextLine());
			if(berat > 150 || berat < 1){
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				return;
			}
			
			System.out.print("Jumlah Anggota Keluarga: ");
			byte makanan = Byte.parseByte(input.nextLine());
			if(makanan > 20 || makanan < 1){
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				return;
			}
			
			System.out.print("Tanggal Lahir          : ");
			String tanggalLahir = (input.nextLine());
			String[] partisiTanggal = tanggalLahir.split("-");
			short tahunLahir = Short.parseShort(partisiTanggal[2]);
			if(tahunLahir > 2017 || tahunLahir < 1001){
				System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
				return;
			}
			
			System.out.print("Catatan Tambahan       : ");
			String catatan = input.nextLine();
			System.out.print("Jumlah Cetakan Data    : ");
			byte jumlahCetakan = Byte.parseByte(input.nextLine());
			System.out.println();
		


			// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
			// TODO Hitung rasio berat per volume (rumus lihat soal)
			float rasio = (berat*1000000)/(panjang*lebar*tinggi);

			for (byte nomorCetakan = 0; nomorCetakan < jumlahCetakan; nomorCetakan++) {
				// TODO Minta masukan terkait nama penerima hasil cetak data
				System.out.print("Pencetakan " + (nomorCetakan+1) + " dari " + jumlahCetakan + " untuk: ");
				String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

				// TODO Periksa ada catatan atau tidak
				if (catatan.equals("")) catatan = "Tidak ada catatan tambahan";
				else catatan = catatan;

				// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
				String hasil = (nama + " - " + alamat + "\n" +
				"Lahir pada tanggal " + tanggalLahir + "\n" +
				"Rasio berat per volume     = " + rasio + " kg/m^3\n" +
				catatan + "\n");
				
				System.out.println("DATA SIAP DICETAK UNTUK " + penerima + '\n'+
				"--------------------\n" +
				hasil);
			}


			// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
			// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)


			
			// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
			int totalASCII = 0;
			for(byte index = 0; index<nama.length(); index++){
				int tempTotalASCII = nama.charAt(index);
				totalASCII += tempTotalASCII;
				
			}
			String nomorKeluarga = (nama.charAt(0) + String.valueOf((((panjang*tinggi*lebar) + totalASCII) % 10000))) ;

			// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
			int anggaran = 50000*365*makanan;

			// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
			short umur = (short) (2018-tahunLahir); // lihat hint jika bingung

			// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
			String apartemen = "";
			String kabupaten = "";
			
			if(0<umur & umur<19){
				apartemen = "PPMT";
				kabupaten = "Rotunda";
				
			}else if(0<anggaran & anggaran < 100000000){
				apartemen = "Teksas";
				kabupaten = "Sastra";
			
			}else{
				apartemen = "Mares";
				kabupaten = "Margonda";
			}


			// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
			String rekomendasi = "REKOMENDASI APARTEMEN \n" +
			"MENGETAHUI : Identitas Keluarga: " + nama + " - " + nomorKeluarga +"\n"+
			"MENIMBANG  : Anggaran makanan tahunan : Rp " + anggaran + '\n' +
			"Umur kepala keluarga     : " + umur + " Tahun\n" +
			"MEMUTUSKAN	: Keluarga " + nama + " akan ditempatkan di: \n"+
			apartemen + ", Kabupaten " + kabupaten;
			System.out.println(rekomendasi);

			input.close();
		}
		catch (NumberFormatException e){
			System.out.println("WARNING : Keluarga ini tidak perlu direlokasi!");
		
	}
	}
}