public class Manusia {
    private String nama;
    private int umur;
    private int uang;
    private double kebahagiaan = 50.0;
    private static double maxKebahagiaan = 100.0;
    private boolean meninggal = false;
    private static Manusia orangTerakhir;
    private static String outPerintahMeninggal = "";

    public boolean getMeninggal(){
        return meninggal;
    }

    public Manusia(String nama, int umur){
        this.nama = nama;
        this.umur = umur;
        this.uang = 50000;
        this.orangTerakhir = this;
    }

    public Manusia(String nama, int umur, int uang){
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.orangTerakhir = this;
    }

    public void setNama(String name){
        if(meninggal){
            System.out.println(name + " sudah meninggal. Nama tidak bisa dirubah untuk menghindari azab kubur");
        }
        this.nama = name;
    }

    public String getNama(){
        return this.nama;
    }

    public void meninggal() {
        if (meninggal){
            System.out.println(getNama() + "  sudah meninggal.");
        }
        else {
            meninggal = true;
            System.out.println(nama + " meninggal dengan tenang, kebahagiaan : " + kebahagiaan);
            if (orangTerakhir.equals(this)) {
                System.out.println("Semua harta " + nama + " hangus");
            } else {
                System.out.println("Semua harta " + nama + "disumbangkan untuk" + orangTerakhir.getNama());
                orangTerakhir.setUang(orangTerakhir.getUang() + uang);
            }
            uang = 0;
        }
    }

    public void setUmur(int umur){
        if(meninggal) {
            System.out.println(nama + " sudah meninggal. Pencatatan umur tidak bisa dirubah");
        }
        else{
            this.umur = umur;
        }
    }

    public int getUmur(){
        return this.umur;
    }

    public void setUang(int uang){
        if(meninggal) {
            System.out.println(nama + " sudah meninggal. Uang tidak bisa dibawa ke alam kubur");
        }
        else{
            this.uang = uang;
        }
    }

    public int getUang(){
        return this.uang;
    }

    public void setKebahagiaan(double kebahagiaan){
        if(meninggal) {
            System.out.println(nama + " sudah meninggal. Kebahagiaan di dunia tidak bisa dibawa ke alam kubur");
        }
        else{
            this.kebahagiaan = kebahagiaan;
        }
    }

    public double getKebahagiaan(){
        return this.kebahagiaan;
    }

    public String toString(){
        if (meninggal) {
            String name = "Almarhum " + getNama();
        }
        String name = getNama();
        return "Nama \t\t:" + name + "\n"
                + "Umur \t\t:" + getUmur() + "\n"
                + "Uang \t\t:" + getUang() + "\n"
                + "Kebahagiaan \t:" + getKebahagiaan();
    }

    public void sakit(String namaPenyakit){
        if(meninggal) {
            System.out.println(nama + " terkena azab " + namaPenyakit);
        }
        else {
            kebahagiaan = kebahagiaan - namaPenyakit.length();
            if (kebahagiaan < 0) {
                kebahagiaan = 0.0;
            }
            System.out.println(nama + " terkena penyakit " + namaPenyakit + " :O");
        }
    }

    public void rekreasi(String namaTempat){
        if(meninggal) {
            System.out.println(nama + " bangkit dari alam kubur untuk ke " + namaTempat + ". Tetapi tidak bisa menikmatinya");
        }
        else {
            int lenTempat = namaTempat.length();
            int biaya = lenTempat * 10000;
            if (uang < biaya) {
                System.out.println(nama + " tidak mempunyai uang untuk berekreasi di " + namaTempat);
            }
            else{
                kebahagiaan += namaTempat.length();
                System.out.println(nama + " berekreasi di " + namaTempat + ", " + nama + " senang :)");
                uang -= biaya;
            }
            if (kebahagiaan > 100.0) {
                kebahagiaan = 100.0;
            }
        }
    }

    public void bekerja(int durasi, int bebanKerja){
        if(meninggal) {
            System.out.println(nama + " sudah meninggal.");
        }
        else {
            if (umur < 18) {
                System.out.println(nama + " belum boleh bekerja karena dibawah umur D:");
            } else {
                int bebanKerjaTotal = bebanKerja * durasi;
                if (bebanKerjaTotal > kebahagiaan) {
                    durasi = (int) (kebahagiaan / bebanKerja);
                    bebanKerjaTotal = durasi * bebanKerja;
                    int pendapatan = bebanKerjaTotal * 10000;
                    uang += pendapatan;
                    kebahagiaan -= bebanKerjaTotal;
                    System.out.println(nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
                } else {
                    int pendapatan = bebanKerjaTotal * 10000;
                    uang += pendapatan;
                    kebahagiaan -= bebanKerjaTotal;
                    System.out.println(nama + " bekerja full time, total pendapatan : " + pendapatan);
                }
                System.out.println("Jumlah uang ditambahkan dengan pendapatan");
            }
        }
    }

    public void beriUang(Manusia penerima) {
		int jumlahASCII = 0;
        String namaPenerima = penerima.getNama();
        int lenPenerima = namaPenerima.length();
        double kebahagiaanPenerima = penerima.getKebahagiaan();
        int uangPenerima = penerima.getUang();
        if (meninggal | penerima.getMeninggal()) {
            if(meninggal){
                System.out.println(nama + " sudah meninggal. Di alam kubur tidak terdapat ATM");
            }
            else{
                System.out.println(penerima.getNama() + "  sudah meninggal. Uang tidak bisa ditukar menjadi pahala");
            }

        }
        else {
            for (int i = 0; i < lenPenerima; i++) {
                jumlahASCII += (int) namaPenerima.charAt(i);
            }
            int jumlahUang = jumlahASCII * 100;
            beriUang(penerima, jumlahUang);
        }
    }

    public void beriUang(Manusia penerima, int jumlahUang) {
        String namaPenerima = penerima.getNama();
        double kebahagiaanPenerima = penerima.getKebahagiaan();
        int uangPenerima = penerima.getUang();
        if (meninggal | penerima.getMeninggal()) {
            if (meninggal) {
                System.out.println(nama + " sudah meninggal. Di alam kubur tidak terdapat ATM");
            } else {
                System.out.println(namaPenerima + "  sudah meninggal. Uang tidak bisa ditukar menjadi pahala");
            }

        }
        else {
            if (jumlahUang > uang) {
                System.out.println(nama + " ingin memberi uang kepada " + namaPenerima + " namun tidak memiliki cukup uang :'(");
            } else {
                System.out.println(nama + " memberi uang sebanyak " + jumlahUang + " kepada " + namaPenerima + ", mereka berdua senang :D");
                uang -= jumlahUang;
                penerima.setUang(uangPenerima + jumlahUang);
                kebahagiaan += (((double) jumlahUang) / 6000);
                penerima.setKebahagiaan(kebahagiaanPenerima + ((double) jumlahUang) / 6000);
                kebahagiaanPenerima = penerima.getKebahagiaan();
                if (kebahagiaan > maxKebahagiaan) {
                    kebahagiaan = maxKebahagiaan;
                }
                if (kebahagiaanPenerima > maxKebahagiaan) {
                    penerima.setKebahagiaan(maxKebahagiaan);
                }
            }
        }
    }
}