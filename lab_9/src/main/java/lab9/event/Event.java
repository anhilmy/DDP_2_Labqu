package lab9.event;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
* A class representing an event and its properties
*/
public class Event
{
    /** Name of event */
    private String name;
    /** Objek date, kapan dia mulai dan kapan dia selesai */
    private Date start;
    private Date end;
    /** type Big Integer karena harga yang tidak karuan */
    private BigInteger cost;
    /** format masukkan untuk waktu mulai dan selesai */
    private SimpleDateFormat formatterIn = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    private SimpleDateFormat formatterOut = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");

    /**
     * Constructor Event
     * @param name              nama Event
     * @param mulai             waktu mulai
     * @param selesai           waktu selesai
     * @param harga             harga acara
     * @throws ParseException   kalau waktu selesai dan waktu mulai tidak sesuai dengan format
     */
    public Event(String name, String mulai, String selesai, String harga) throws ParseException {
        this.name = name;
        this.start = formatterIn.parse(mulai);
        this.end = formatterIn.parse(selesai);
        this.cost = new BigInteger(harga);
        //kalau start setelah end
        if(end.before(start)) throw new ParseException("waktu salah", 1);
    }

    public BigInteger getCost() {
        return cost;
    }

    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }

    /**
     * getter waktu mulai
     * @return
     */
    public Date getStart() {
        return start;
    }

    /**
     * getter waktu selesai
     * @return
     */
    public Date getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return (name + "\n" +
                "Waktu mulai: " + formatterOut.format(start) + "\n" +
                "Waktu selesai: " + formatterOut.format(end) + "\n" +
                "Biaya kehadiran: " + cost);
    }

    /**
     * cek kalau dia tabrakan sama event lain
     * @param other     event lain
     * @return boolean kalau dia tabrakan sama event lain
     */
    public boolean isOverlapWith(Event other){
        Date otherEnd = other.getEnd();
        Date otherStart = other.getStart();
        if(this.start.after(otherEnd) || this.end.before(otherStart)){
            return false;
        }
        return true;
    }
}
