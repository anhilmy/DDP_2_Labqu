package lab9.user;

import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;


/**
* Class representing a user, willing to attend event(s)
*/
public class User
{
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private ArrayList<Event> events;
    
    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }
    
    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events. then sort the List by start time
    *
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent)
    {
        for (Event acaraYangAda: events) {
            if(acaraYangAda.isOverlapWith(newEvent)){
                return false;
            }
        }
        events.add(newEvent);
        events.sort(Comparator.comparing(Event::getStart));
        return true;
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents()
    {
        // WARNING: The list returned needs to be a copy of the actual events list.
        //          You don't want people to change your plans (e.g. clearing the
        //          list) without your consent, right?
        // HINT: see Java API Documentation on ArrayList (java.util.ArrayList)                                                                                                          (or... Google. Yeah that works too. OK.)
        return (ArrayList<Event>) events.clone();
    }

    /**
     * total harga yang dibutuh kan oleh user untuk mendatangi semua acaranya
     * @return
     */
    public BigInteger getTotalCost() {
        BigInteger totalCost = BigInteger.valueOf(0);
        for (Event eleAcara: events) {
            totalCost = totalCost.add(eleAcara.getCost());
        }
        return totalCost;
    }
}
