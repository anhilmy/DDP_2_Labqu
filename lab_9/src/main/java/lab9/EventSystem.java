package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.text.ParseException;
import java.util.ArrayList;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * membuat method find user karena akan dipakai berkali kali
     * @param nameUser  nama user yang perlu dicari
     * @return objek kalau ada, null kalau tidak ada
     */
    private User findUser(String nameUser){
        for (User aUser:users){
            if(aUser.getName().equalsIgnoreCase(nameUser)){
                return aUser;
            }
        }
        return null;
    }

    /**
     * method find Event karena akan dipakai di beberapa method
     * @param namaEvent     nama event yang perlu dicari
     * @return  objek kalau ada, null kalau tidak ada
     */
    private Event findEvent(String namaEvent){
        for (Event aEvent:events){
            if(aEvent.getName().equalsIgnoreCase(namaEvent)){
                return aEvent;
            }
        }
        return null;
    }

    /**
     * addEvent, kemungkinan nama sudah ada, atau belum ada
     *
     * @param name
     * @param startTimeStr
     * @param endTimeStr
     * @param costPerHourStr
     * @return
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        Event aEvent = findEvent(name);
        //cek apakah sudah ada event dengan nama yang sama
        if(aEvent!=null) {
            return "Event " + name + " sudah ada!";
        }

        //add ke arraylist event
        try {
            events.add(new Event(name, startTimeStr, endTimeStr, costPerHourStr));
        } catch (ParseException e){ //kalau format tanggal tidak sesuai
            return "Waktu yang diinputkan tidak valid!";
        }
        return "Event " + name + " berhasil ditambahkan!";
    }

    /**
     * addUser, kemungkinan nama sudah ada atau belum ada
     * @param name      tambah user dengan nama
     * @return
     */
    public String addUser(String name)
    {
        User aUser = findUser(name);
        //kalau udah ada
        if(aUser!=null){
            return "User " + name + " sudah ada!";
        }
        //kalau belum ada
        users.add(new User(name));
        return "User " + name + " berhasil ditambahkan!";
    }
    
    public String registerToEvent(String userName, String eventName)
    {
        User aUser = findUser(userName);
        Event aEvent = findEvent(eventName);

        //kalau user dan acara tidak ada
        if(aUser==null && aEvent==null){
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        }
        //kalau user tidak ada
        else if(aUser==null){
            return "Tidak ada pengguna dengan nama " + userName + "!";
        }
        //kalau nama event tidak ada
        else if(aEvent==null){
            return "Tidak ada event dengan nama " + eventName;
        }
        //kalau bisa di add
        else if(aUser.addEvent(aEvent)){
            return userName + " berencana menghadiri " + eventName + "!";
        }
        //kalau tidak bisa di add
        else {
            return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
        }
    }

    public String getEvent(String event) {
        Event aEvent = findEvent(event);
        if(aEvent!=null)
            return (aEvent.toString());
        else{
            return ("tidak ada event");
        }
    }

    public User getUser(String namaUser) {
        return findUser(namaUser);
    }
}