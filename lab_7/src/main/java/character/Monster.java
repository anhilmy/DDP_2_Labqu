package character;

public class Monster extends Player{
    String roar = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster(String nama, int hp){
        super(nama, hp*2, "Monster");
    }

    public Monster(String nama, int hp, String roar){
        super(nama, hp*2, "Monster");
        this.roar = roar;
    }

    public String roar(){
        return this.roar;
    }
}