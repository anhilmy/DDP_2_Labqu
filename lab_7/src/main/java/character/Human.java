package character;

public class Human extends Player {

    public Human(String nama, int hp) {
        super(nama, hp, "Human");
    }

    public Human(String nama, int hp, String type) {
        super(nama, hp, type);
    }
}