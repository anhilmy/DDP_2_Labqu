package character;

public class Magician extends Human{

    public Magician(String nama, int hp) {
        super(nama, hp, "Magician");
    }

    public String burn(Player target) {
        int damage = 10;
        if(target instanceof Magician){
            damage = 20;
        }
        if (target.getHp() <= damage) {
            target.setBurned(true);
            target.setHp(0);
            return "Nyawa " + target.getName() + " 0 \n" +
                    "dan matang";
        }
        int reducedHp = target.getHp() - damage;
        target.setHp(reducedHp);
        return "Nyawa " + target.getName() + " "+ target.getHp();

    }
}