package character;

import java.util.ArrayList;

public class Player{
    private String nama;
    private int hp;
    private ArrayList<String> diet = new ArrayList<>(0);
    private String type;
    private boolean isBurned;

    Player(String nama,int hp, String type){
        this.nama = nama;
        this.hp = hp;
        this.type = type;
    }

    public void attack(Player target) {
        if(target instanceof Magician){
            int hpReduced = target.getHp() - 20;
            target.setHp(hpReduced);
        }
        else{
            int hpReduced = target.getHp() - 10;
            target.setHp(hpReduced);
        }
    }

    public String eat(Player target){
        diet.add(target.getType() + " " + target.getName());
        this.hp += 15;
        return nama + " memakan " + target.getName() + "\n" +
                "Nyawa " + nama + " kini " + hp;
    }

    public String status() {
        String output = "";
        output += type + " " + nama + "\n" +
                "HP: " + hp + "\n";
        if (hp > 0) {
            output += "Masih hidup\n";
        } else {
            output += "Sudah meninggal dunia dengan damai\n";
        }

        if (diet.size() > 0) {
            output += "Memakan " + getDiet();
        } else {
            output += "Belum memakan siapa siapa";
        }
        return output;
    }

    public boolean canEat(Player target){
        if(this instanceof Human) {
            if (target.getBurned() && target.getHp() < 1 && target.getType().equals("Monster")) {
                return true;
            }
            return false;
        }
        else{
            if(target.getHp() < 1){
                return true;
            }
            return false;
        }
    }

    public String getName() { return this.nama; }

    public int getHp() { return this.hp; }

    public String getDiet() {
        String stringDiet = "";
        int panjangDiet = diet.size();
        for (int i = 0; i < panjangDiet; i++) {
            stringDiet += diet.get(i);
            if(!(i+1==panjangDiet)){
                stringDiet += ", ";
            }
        }
        return stringDiet;
    }

    public String getType() { return this.type; }

    protected void setHp(int hp) {
        this.hp = hp;
        if(this.hp<0){
            this.hp = 0;
        }
    }

    protected boolean getBurned() { return this.isBurned; }

    protected void setBurned(boolean burned) { this.isBurned = burned; }
}