import java.util.ArrayList;
import character.*;


public class Game{

    ArrayList<Player> player = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     * @param "String" name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player aPlayer : player) {
            String namaPlayer = aPlayer.getName();
            if (namaPlayer.equals(name)) {
                return aPlayer;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param "String" chara nama karakter yang ingin ditambahkan
     * @param "String" tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param "int" hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if(find(chara)==null) {
            switch (tipe) {
                case "Magician":
                    player.add(new Magician(chara, hp));
                    break;
                case "Human":
                    player.add(new Human(chara, hp));
                    break;
                case "Monster":
                    player.add(new Monster(chara, hp));
                    break;
            }
            return chara +" ditambah ke game";
        }
        return ("Sudah ada karakter bernama " + chara);
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param "String" chara nama karakter yang ingin ditambahkan
     * @param "String" tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param "String" hp hp dari karakter yang ingin ditambahkan
     * @param "String" roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if(!(find(chara)==null)) {
            switch (tipe) {
                case "Monster":
                    player.add(new Monster(chara, hp, roar));
                    break;
                default: return tipe + " tidak bisa berteriak";
            }
            return chara +" ditambah ke game";
        }
        return ("Sudah ada karakter bernama " + chara);
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param "String" chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        Player removed = find(chara);

        if(player.remove(removed)) {
            return chara + " dihapus dari game";
        }
        return "Tidak ada " + chara;
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param "String" chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        Player aPlayer = find(chara);
        if(!(aPlayer==null)){
            return aPlayer.status();
        }
        return "Tidak ada " + chara;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String allStatus = "";
        if(player.size()>0) {
            for (Player aPlayer : player) {
                allStatus += aPlayer.status();
            }
            return allStatus;
        }
        return "Tidak ada pemain";
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param "String" chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        Player aPlayer = find(chara);
        if(!(aPlayer==null)){
            return aPlayer.getDiet();
        }
        return "Tidak ada " + chara;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String dietString = "";
        if(player.size()>0) {
            for (Player aPlayer : player) {
                dietString += aPlayer.getDiet() + "\n";
            }
            return dietString;
        }
        return "Tidak ada pemain";
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param "String" meName nama dari character yang sedang dimainkan
     * @param "String" enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player attacker = find(meName);
        Player attacked = find(enemyName);
        if(!(attacker==null) && !(attacked==null)){
            attacker.attack(attacked);
            return "Nyawa " + attacked.getName() + " " + attacked.getHp();
        }
        return "Tidak ada " + meName + " atau " + enemyName;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param "String" meName nama dari character yang sedang dimainkan
     * @param "String" enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        try {
            Magician attacker = (Magician) find(meName);
            Player attacked = find(enemyName);
            if (!(attacker == null) && !(attacked == null)) {
                return attacker.burn(attacked);
            }
            return "Tidak ada " + meName + " atau " + enemyName;
        } catch (ClassCastException e){
            return meName + " tidak bisa melakukan burn";
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param "String" meName nama dari character yang sedang dimainkan
     * @param "String" enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player attacker = find(meName);
        Player attacked = find(enemyName);
        if(!(attacker==null) && !(attacked==null)){
            if(attacker.canEat(attacked)){
                player.remove(attacked);
                return attacker.eat(attacked);
            }
            return meName + " tidak bisa memakan " + enemyName;
        }
        return "Tidak ada " + meName + " atau " + enemyName;

        /**cara deciding pembedaan eatnya human sama monster, karena butuh parameter yang beda untuk canEat() nya
         Karena kritetia canEat untuk human adalah HP<0 !Human isBurned
         dan kriteria canEat untuk monster adalah HP<0
         **/

    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param "String" meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        try{
            Monster aPlayer = (Monster) find(meName);
            if(aPlayer == null){
                return "Tidak ada " + meName;
            }
            return aPlayer.roar();
        } catch (ClassCastException e){
            return meName + " tidak bisa melakukan roar";
        }
    }
}