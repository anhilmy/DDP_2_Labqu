package ticket;

import movie.Movie;

public class Ticket {
    private Movie film;
    private String hari;
    private String tridimensi;
    private int harga = 60000;

    public Ticket(Movie film, String hari, boolean boolTridimensi){
        this.film = film;
        this.hari = hari;
        if (boolTridimensi) {
            this.tridimensi = "3 Dimensi";
        }
        else {
            this.tridimensi = "Biasa";
        }

        if(hari.equals("Minggu") || hari.equals("Sabtu")){
            harga += 40000;
        }

        if(boolTridimensi){
            harga *= 1.2;
        }
    }

    public String getHari(){
        return this.hari;
    }

    public Movie getMovie(){
        return this.film;
    }

    public int getHarga(){
        return this.harga;
    }

    public String getTridimensi(){
        return this.tridimensi;
    }

    public void printInfo(){
        System.out.print("------------------------------------------------------------------\n"
                + "Film\t\t: " + film.getJudul() + "\n"
                + "Jadwal Tayang\t: " + this.hari + "\n"
                + "Jenis\t\t: " + this.tridimensi + "\n"
                + "------------------------------------------------------------------/n");
    }


}
