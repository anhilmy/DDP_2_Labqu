package customer;

import movie.Movie;
import theater.Theater;
import ticket.Ticket;

public class Customer {
    private String nama;
    private int umur;

    public Customer(String nama, boolean gender, int umur){
        this.nama = nama;
        this.umur = umur;
    }

    public Ticket orderTicket(Theater bioskop, String judul, String hari, String tridimensi){
        Ticket tiket = bioskop.orderTicket(judul, hari, tridimensi);

        if(tiket==null){
            System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s\n", judul, tridimensi, hari, bioskop.getNama());
        }
        else {
            Movie film = tiket.getMovie();
            if (this.umur < film.getUmur()) {
                System.out.printf("%s masih belum cukup umur untuk menonton %s dengan rating %s\n", this.nama, film.getJudul(), film.getRating());
            } else {
                System.out.printf("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d\n", this.nama, film.getJudul(), tridimensi, bioskop.getNama(), hari, tiket.getHarga());
                bioskop.tambahKas(tiket);
            }
        }
        return tiket;
    }

    public void findMovie(Theater bioskop, String judul){
        if(!bioskop.findMovie(judul)){
            System.out.printf("Film %s yang dicari %s tidak ada di bioskop %s\n", judul, this.nama, bioskop.getNama());
        }
    }

}
