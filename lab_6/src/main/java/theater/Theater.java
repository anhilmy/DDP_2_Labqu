package theater;

import movie.Movie;
import ticket.Ticket;
import java.util.ArrayList;

public class Theater {
    private String nama;
    private int kas;
    private ArrayList<Ticket> tiket;
    private Movie[] film;
    private int jumlahTiket;
    private int jumlahFilm;


    public Theater(String nama, int kas, ArrayList<Ticket> tiket, Movie[] film){
        this.nama = nama;
        this.kas = kas;
        this.tiket = tiket;
        this.film = film;
        this.jumlahTiket = tiket.size();
        this.jumlahFilm = film.length;

    }

    public Ticket orderTicket(String judul, String hari, String tridimensi){
        for (Ticket tic : tiket){
            if(tic.getMovie().getJudul().equals(judul) && tic.getHari().equals(hari) && tic.getTridimensi().equals(tridimensi)){
                return tic;
            }
        }
        return null;
    }

    public boolean findMovie(String judul){
        for (Ticket tic : tiket){
            if (tic.getMovie().getJudul().equals(judul)){
                tic.getMovie().printInfo();
                return true;
            }
        }
        return false;
    }

    public static void printTotalRevenueEarned(Theater[] theater) {
        int tot = 0;
        for (Theater thea : theater) {
            tot += thea.getKas();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + tot);
        System.out.println("------------------------------------------------------------------");
        for (Theater thea : theater) {
            System.out.println("Bioskop\t\t: " + thea.getNama() + "\n"
                    + "Saldo Kas\t:  Rp. " + thea.getKas() + "\n");
        }
        System.out.println("------------------------------------------------------------------");
    }

    public String getNama(){
        return this.nama;
    }

    public int getKas(){
        return this.kas;
    }

    public void tambahKas(Ticket tiket){
        this.kas += tiket.getHarga();
    }

    public void printInfo(){
        System.out.print("------------------------------------------------------------------\n"
                + "Bioskop\t\t\t: " + this.nama + "\n"
                + "Saldo Kas\t\t: " + this.kas + "\n"
                + "Jumlah tiket tersedia\t: " + this.jumlahTiket + "\n"
                + "Daftar Film tersedia\t: ");

        for(int i = 0; i < jumlahFilm; i++){
            System.out.print(film[i].getJudul());

            if((i+1)!=jumlahFilm){
                System.out.print(", ");
            }
        }
        System.out.print("\n------------------------------------------------------------------\n");
    }
}
