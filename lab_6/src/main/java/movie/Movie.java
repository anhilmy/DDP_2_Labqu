package movie;

public class Movie {
    private String genre;
    private int durasi;
    private String jenis;
    private String rating;
    private String judul;
    private int untukUmur = 15;

    public Movie(String judul, String rating, int durasi, String genre,  String jenis){
        this.judul = judul;
        this.genre = genre;
        this.durasi = durasi;
        this.rating = rating;
        this.jenis = jenis;
        this.setUntukUmur();
        /*
        switch(rating) {
            case "Dewasa": this.untukUmur = 17;
            case "Remaja": this.untukUmur = 15;
            case "Umum" : this.untukUmur = 0;
        }
        */
    }

    private void setUntukUmur(){
        if(rating.equals("Dewasa")){
            this.untukUmur = 17;
        }else if(rating.equals("Remaja")){
            this.untukUmur = 15;
        }else{
            this.untukUmur = 0;
        }
    }
    public int getUmur() {
        return this.untukUmur;
    }

    public String getJudul() {
        return this.judul;
    }

    public String getRating() {
        return this.rating;
    }
    public void printInfo(){
        System.out.print("------------------------------------------------------------------\n"
                + "Judul\t: " + this.judul + "\n"
                + "Genre\t: " + this.genre + "\n"
                + "Durasi\t: " + this.durasi + " menit\n"
                + "Rating\t: " + this.rating + "\n"
                + "Jenis\t: Film " + this.jenis + "\n"
                + "------------------------------------------------------------------\n");
    }
}
