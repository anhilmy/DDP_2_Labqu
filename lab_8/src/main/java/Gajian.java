import Employee.Intern;
import Employee.Karyawan;
import Employee.Manager;
import Employee.Staff;

import java.util.Scanner;

public class Gajian {
    static public void main(String[] args){
        Scanner in = new Scanner(System.in);
        Corp Korps = new Corp();
        Staff.setMaxGaji(in.nextInt());
        String[] input;
        Karyawan karyawanTemp;
        Karyawan karyawanBawahan;
        int gajiAwal;
        String namaKaryawan;

        while(in.hasNext()){
            input = in.nextLine().split(" ");

            if(input[0].equals("TAMBAH_KARYAWAN")) {
                if (Korps.getKaryawanSize() < 1000) {
                    namaKaryawan = input[1];
                    gajiAwal = Integer.parseInt(input[3]);
                    if(Korps.find(namaKaryawan)==null) {
                        switch (input[2]) {
                            case "MANAGER":
                                karyawanTemp = new Manager(namaKaryawan, gajiAwal);
                                Korps.addKaryawan(karyawanTemp);
                                break;
                            case "STAFF":
                                karyawanTemp = new Staff(namaKaryawan, gajiAwal);
                                Korps.addKaryawan(karyawanTemp);
                                break;
                            case "INTERN":
                                karyawanTemp = new Intern(namaKaryawan, gajiAwal);
                                Korps.addKaryawan(karyawanTemp);
                                break;
                        }
                    }
                    else{
                        System.out.println("Karyawan dengan nama "+ namaKaryawan +" sudah ada");
                    }
                }
                else{
                    System.out.println("Karyawan sudah banyak");
                }
            }


            else if(input[0].equals("STATUS")) {
                karyawanTemp = Korps.find(input[1]);
                try {
                    karyawanTemp.status();
                } catch (NullPointerException e) {
                    System.out.println("Karyawan tidak ditemukan");
                }
            }


            else if(input[0].equals("TAMBAH_BAWAHAN")) {
                karyawanTemp = Korps.find(input[2]);
                karyawanBawahan = Korps.find(input[1]);
                if (karyawanTemp != null && karyawanBawahan != null) {
                    System.out.println(karyawanTemp.addBawahan(karyawanBawahan));
                } else {
                    System.out.println("Nama tidak berhasil ditemukan");
                }
            }

            else if(input[0].equals("GAJIAN")){
                Korps.gajian();
            }

        }
    }
}
