import Employee.Karyawan;
import Employee.Manager;

import java.util.ArrayList;
import java.util.List;

public class Corp {
    private List<Karyawan> karyawan = new ArrayList<>();
    private final int THRESHOLD_KARYAWAN = 10000;


    /**
     * Mencari seorang karyawan di korporasi
     * bisa buat ngecek bisa nambah orang atau tidak, atau tambah bawahan
     * @param nama
     * @return
     */
    public Karyawan find(String nama){
        for (Karyawan emplyTampan: karyawan ) {
            if(emplyTampan.getNama().equals(nama)){
                return emplyTampan;
            }
        }
        return null;
    }

    /**
     * Nambahin karyawan ke List karyawan
     * @param employee
     */

    public void addKaryawan(Karyawan employee){
        karyawan.add(employee);
        System.out.println(employee.getNama() + " mulai bekerja sebagai " + employee.getType() + " di PT. TAMPAN");
    }


    /**
     * pengecekan kalau masih dibawah 10000
     * @return jumlah karyawannya
     */
    public int getKaryawanSize(){
        return this.karyawan.size();
    }

    /**
     * looping buat semua karyawan yang akan diberikan gaji
     */

    public void gajian(){
        System.out.println("Semua karyawan telah diberikan gaji");
        List<Karyawan> bisaNaik = new ArrayList<>();
        for (Karyawan aKaryawan:karyawan) {
            aKaryawan.gajian();
            if(aKaryawan.naikPangkat()){
                bisaNaik.add(aKaryawan);
            }
        }

        /**
         * Untuk mengecek, setalah gajian ada yang bisa naik pangkat atau tidak
         * kalau ada, dia bakal remove yang bisa naik pngkat dari list karyawan dan dihilngkan
         * dari list bawahan.
         * Setelah itu, dia bakal di add ke list karyawan dengan objek manager dan bawa list bawahan yang dimiliki
         *
         */
        if(bisaNaik.size()>0) {
            for (Karyawan aKaryawan : bisaNaik) {
                karyawan.remove(aKaryawan);
            }

            for (Karyawan aKaryawan : karyawan) {
                for (Karyawan naikJabatan : bisaNaik) {
                    aKaryawan.getDaftarBawahan().remove(naikJabatan);
                }
            }

            for (Karyawan aKaryawan:bisaNaik) {
                karyawan.add(new Manager(aKaryawan.getNama(), (int) aKaryawan.getGajiAwal(), aKaryawan.getDaftarBawahan()));
            }
        }


    }
}
