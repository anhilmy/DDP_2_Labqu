package Employee;

public class Staff extends Karyawan{
    static int THRESHOLD_GAJI;
    private boolean isManagerNow = false;

    public Staff(String nama, int gajiAwal) {
        super(nama, gajiAwal);
    }

    /**
     * tambah bawahan, bawahan yang dimiliki ga boleh lebih dari 10
     * bawahannya adalah intern
     * kalau bawahannya manager atau staff manager atau staf biasa tidak bisa
     * @param bawahan
     * @return String agar di print di main
     */
    @Override
    public String addBawahan(Karyawan bawahan) {
        if (this.isBawahanUnder10()) {
            if (bawahan instanceof Intern) {
                if (findBawahan(bawahan)) {
                    setBawahan(bawahan);
                    return "Karyawan " + bawahan.getNama() + " berhasil ditambahkan menjadi bawahan " + this.getNama();
                }
                return "Karyawan " + bawahan.getNama() + " telah menjadi menjadi bawahan " + this.getNama();
            }
            return "Tidak layak memiliki bawahan";
        }
        return "Bawahan sudah 10";
    }


    /**
     * untuk print status
     * @return
     */
    @Override
    public String getType() {
        return "STAFF";
    }

    /**
     * set max gaji staff pas awal awal
     * @param gaji
     */
    public static void setMaxGaji(int gaji){
        THRESHOLD_GAJI = gaji;
    }

    /**
     * apakah dia udah jadi manager?
     * @return
     */
    public boolean getManager(){
        return this.isManagerNow;
    }

    /**
     * karena gajian nya staff perlu ngecek apakah udah melewai maksimal gaji
     */
    public void gajian(){
        super.gajian();
        if(getGajiAwal()>THRESHOLD_GAJI){
            this.isManagerNow = true;
            System.out.println("Selamat, " + getNama() +" telah dipromosikan menjadi MANAGER");
        }
    }

    @Override
    /**
     * untuk staff, kalau dia bisa naik pangkat return true.
     */
    public boolean naikPangkat(){
        if(this.getGajiAwal() > THRESHOLD_GAJI){
            return true;
        }
        return false;
    }
}
