package Employee;

import java.util.List;

public class Manager extends Karyawan {

    public Manager(String nama, int gajiAwal) {
        super(nama, gajiAwal);
    }

    /**
     * untuk iniasiasi orang yang naik pangkat
     * @param nama
     * @param gajiAwal
     * @param daftarBawahanBawaan
     */
    public Manager(String nama, int gajiAwal, List<Karyawan> daftarBawahanBawaan) {
        super(nama, gajiAwal);
        this.setListBawahan(daftarBawahanBawaan);
    }

    /**
     * tambah bawahan, bawahan yang dimiliki ga boleh lebih dari 10
     * bawahannya adalah staff dan bukan manager staff, atau intern baru bisa
     * kalau bawahannya manager atau staff manager tidak bisa
     * @param bawahan
     * @return String agar di print di main
     */
    @Override
    public String addBawahan(Karyawan bawahan) {
        if (this.isBawahanUnder10()) {
            if ((bawahan instanceof Staff) || bawahan instanceof Intern) {
                if (findBawahan(bawahan)) {
                    setBawahan(bawahan);
                    return "Karyawan " + bawahan.getNama() + " berhasil ditambahkan menjadi bawahan " + this.getNama();
                }
                return "Karyawan " + bawahan.getNama() + " telah menjadi menjadi bawahan " + this.getNama();
            }
            return "Tidak layak memiliki bawahan";
        }
        return "Bawahan sudah 10";
    }

    /**
     * untuk print status
     * @return
     */
    @Override
    public String getType() {
        return "MANAGER";
    }
}
