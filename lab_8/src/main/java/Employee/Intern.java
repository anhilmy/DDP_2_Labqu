package Employee;

public class Intern extends Karyawan {
    public Intern(String nama, int gajiAwal) {
        super(nama, gajiAwal);
    }

    /**
     * tidak bisa menambahkan bawahan
     * @param bawahan
     * @return
     */

    @Override
    public String addBawahan(Karyawan bawahan) {
        return "Tidak layak memiliki bawahan";
    }

    /**
     * untuk print status
     * @return
     */
    @Override
    public String getType() {
        return "INTERN";
    }
}
