package Employee;

import java.util.ArrayList;
import java.util.List;

public abstract class Karyawan {
    private final int THRESHOLD_BAWAHAN = 10;
    private double gajiAwal;
    private String nama;
    private int gajiKe = 0;
    private List<Karyawan> daftarBawahan= new ArrayList<>();

    public Karyawan(String nama, int gajiAwal){
        this.nama = nama;
        this.gajiAwal = gajiAwal;
    }

    /**
     * untuk staff check kalau melebih threshold
     * @return
     */
    public double getGajiAwal(){
        return this.gajiAwal;
    }

    /**
     * untuk print nama dimanapun
     * @return
     */
    public String getNama(){
        return this.nama;
    }

    public void status(){
        System.out.println(nama + " " + (int) gajiAwal);
    }

    /**
     * untuk menambah bawahan kedalam list
     * @param karyawan
     */
    public void setBawahan(Karyawan karyawan){
        daftarBawahan.add(karyawan);
    }

    /**
     * ngecek apakah udah jadi bawahan, kalo pernah bilang false
     * @param bawahan
     * @return
     */
    public boolean findBawahan(Karyawan bawahan) {
        for (Karyawan seorangBawahan : daftarBawahan) {
            if (seorangBawahan.getNama().equals(bawahan.getNama())) {
                return false;
            }
        }
        return true;
    }

    /**
     * print kenaikan gajian sebesar 10%
     */
    public void gajian(){
        this.gajiKe++;
        if(gajiKe%6 == 0){
            System.out.println(nama +"  mengalami kenaikan gaji sebesar 10% dari " + (int) gajiAwal +
                    " menjadi " + (int) (gajiAwal*1.1));
            gajiAwal = gajiAwal*1.1;
        }
    }

    /**
     * ngecek apakah bawahannya sudah 10
     * @return
     */
    protected boolean isBawahanUnder10(){
        return (daftarBawahan.size() < 10);
    }

    /**
     * semua karyawan tidak bisa naik pangkat kecuali Staff
     * @return
     */
    public boolean naikPangkat(){
        return false;
    }

    /**
     * untuk remove list bawahan kalau ada yang mau naik pangkat
     * @return
     */
    public List<Karyawan> getDaftarBawahan(){
        return this.daftarBawahan;
    }

    /**
     * inisiasi untuk orang yang akan naik pangkat
     * @param bawahanBawaan
     */

    public void setListBawahan(List<Karyawan> bawahanBawaan){
        this.daftarBawahan = bawahanBawaan;
    }

    public abstract String getType();

    public abstract String addBawahan(Karyawan bawahan);
}
