import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class txtreader {

    public String reader() throws FileNotFoundException {

        System.out.print("Enter the file name with extension : ");

        Scanner input = new Scanner(System.in);

        Path test = Paths.get(input.nextLine());
        File file = new File(String.valueOf(test));

        input = new Scanner(file);

        while (input.hasNextLine()) {
            String line = input.nextLine();
            System.out.println(line);
        }
        return null;
    }
}