package xoxo.exceptions;

/**
 * <write the documentation>
 */
public class SizeTooBigException extends RuntimeException {

    /**
     * constructor
     */
    public SizeTooBigException (String message) { super(message); }
    
}