package xoxo.crypto;

import java.util.ArrayList;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        //TODO THROW THROW
        final int length = encryptedMessage.length();
        String message = "";
        int hugLength = hugKeyString.length();
        for(int i = 0; i< length; i++){
           int tahap1 = hugKeyString.charAt(i%hugLength)^seed;
           int tahap2 = tahap1 - 'a';
           int tahap3 = encryptedMessage.charAt(i)^tahap2;
           message += (char) tahap3;
        }

        return message;
    }
}