package xoxo;
import xoxo.crypto.*;
import xoxo.key.HugKey;
import xoxo.key.KissKey;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        setDecrypt();
        setEncrypt();
    }

    private void setEncrypt(){
        gui.setEncryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XoxoEncryption encry = new XoxoEncryption(gui.getKeyText());
                String seed = gui.getSeedText();
                String message = gui.getMessageText();
                String encrypted;
                
                try{
                    encrypted = encry.encrypt(message, Integer.parseInt(seed)).getEncryptedMessage();
                }
                catch (NumberFormatException nfe) {
                    encrypted = encry.encrypt(message).getEncryptedMessage();
                }
                gui.appendLog(encrypted);
            }
        });
    }

    private void setDecrypt(){
        gui.setDecryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //TODO KODE DEKRIPT MASUK SINI
                HugKey hugYou;
                try{
                    hugYou = new HugKey(new KissKey(gui.getKeyText()), Integer.parseInt(gui.getSeedText()));
                }
                catch (NumberFormatException nfe){
                    hugYou = new HugKey(new KissKey(gui.getKeyText()));
                }
                XoxoDecryption decry = new XoxoDecryption(hugYou.getKeyString());
                try {
                    gui.appendLog(decry.decrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText())));
                } catch (NumberFormatException nfe){
                    gui.appendLog(decry.decrypt(gui.getMessageText(), HugKey.DEFAULT_SEED));
                }

            }
        });
    }
}